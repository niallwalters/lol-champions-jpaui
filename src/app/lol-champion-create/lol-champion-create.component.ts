import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-lol-champion-create',
  templateUrl: './lol-champion-create.component.html',
  styleUrls: ['./lol-champion-create.component.css']
})
export class LolChampionCreateComponent implements OnInit {

  @Input() lolChampionDetails = { id: 0, name: '', role: '', difficulty: '' }

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addShipper() {
    this.restApi.createLolChampion(this.lolChampionDetails).subscribe((data: {}) => {
      this.router.navigate(['/lol-champion-list'])
    })
  }

}
