import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionCreateComponent } from './lol-champion-create.component';

describe('LolChampionCreateComponent', () => {
  let component: LolChampionCreateComponent;
  let fixture: ComponentFixture<LolChampionCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolChampionCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
