import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LolChampionCreateComponent } from './lol-champion-create/lol-champion-create.component';
import { LolChampionEditComponent } from './lol-champion-edit/lol-champion-edit.component';
import { LolChampionListComponent } from './lol-champion-list/lol-champion-list.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'lol-champion-list' },
  { path: 'create-lol-champion', component: LolChampionCreateComponent },
  { path: 'lol-champion-list', component: LolChampionListComponent },
  { path: 'lol-champion-edit/:id', component: LolChampionEditComponent },
  { path: '**', component: LolChampionListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
