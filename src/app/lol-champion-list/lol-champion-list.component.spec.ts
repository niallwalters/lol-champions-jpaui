import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionListComponent } from './lol-champion-list.component';

describe('LolChampionListComponent', () => {
  let component: LolChampionListComponent;
  let fixture: ComponentFixture<LolChampionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolChampionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
