import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-lol-champion-list',
  templateUrl: './lol-champion-list.component.html',
  styleUrls: ['./lol-champion-list.component.css']
})
export class LolChampionListComponent implements OnInit {
  LolChampions: any = [];
  constructor(
    public restApi: RestApiService
  ) { }

  ngOnInit(): void {
    this.loadLolChampions()
  }

  loadLolChampions() {
    return this.restApi.getLolChampions().subscribe((data: {}) => {
        this.LolChampions = data;
    })
  }

  deleteLolChampion(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteLolChampion(id).subscribe(data => {
        this.loadLolChampions()
      })
    }
  }
}
