import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LolChampionCreateComponent } from './lol-champion-create/lol-champion-create.component';
import { LolChampionListComponent } from './lol-champion-list/lol-champion-list.component';
import { LolChampionEditComponent } from './lol-champion-edit/lol-champion-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    LolChampionCreateComponent,
    LolChampionListComponent,
    LolChampionEditComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
