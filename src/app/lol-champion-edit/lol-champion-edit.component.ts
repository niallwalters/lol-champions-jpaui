import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-lol-champion-edit',
  templateUrl: './lol-champion-edit.component.html',
  styleUrls: ['./lol-champion-edit.component.css']
})
export class LolChampionEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  lolChampionDetails: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getLolChampion(this.id).subscribe((data: {}) => {
      this.lolChampionDetails = data;
    })
  }

  updateLolChampion() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateLolChampion(this.id, this.lolChampionDetails).subscribe(data => {
        this.router.navigate(['/lol-champion-list'])
      })
    }
  }
}
