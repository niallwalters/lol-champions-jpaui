import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionEditComponent } from './lol-champion-edit.component';

describe('LolChampionEditComponent', () => {
  let component: LolChampionEditComponent;
  let fixture: ComponentFixture<LolChampionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolChampionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
