import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LolChampion } from '../shared/lol-champion';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = environment.apiURL;

  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getLolChampions(): Observable<LolChampion> {
    return this.http.get<LolChampion>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getLolChampion(id:any): Observable<LolChampion> {
    return this.http.get<LolChampion>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  createLolChampion(lolChampion:LolChampion): Observable<LolChampion> {
    return this.http.post<LolChampion>(this.apiURL + '', JSON.stringify(lolChampion), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateLolChampion(id:number, lolChampion:LolChampion): Observable<LolChampion> {
    return this.http.put<LolChampion>(this.apiURL + '', JSON.stringify(lolChampion), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  deleteLolChampion(id:number){
    return this.http.delete<LolChampion>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
